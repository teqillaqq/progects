//
//  Typealias.swift
//  ClevertecFirstApp
//
//  Created by Александр Савков on 7.02.22.
//

import Foundation

typealias CompletionVoid = (() -> Void)
