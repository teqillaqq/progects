//
//  AppDelegate.swift
//  ClevertecFirstApp
//
//  Created by Александр Савков on 4.02.22.
//

import Foundation

extension String {
    
    func localized(loc: String) -> String {
        if let path = Bundle.main.path(forResource: loc, ofType: "lproj"),
           let bundle = Bundle(path: path) {
            return NSLocalizedString(self,
                                     tableName: "Localizable",
                                     bundle: bundle,
                                     value: self,
                                     comment: self)
        } else {
            return NSLocalizedString("Hello!", comment: self)
        }
    }
}
