//
//  UIColor+Extention.swift
//  ClevertecFirstApp
//
//  Created by Александр Савков on 5.02.22.
//

import UIKit

enum AssetsColor : String {
  case button
  case border
}

extension UIColor {
  static func appColor(_ name: AssetsColor) -> UIColor? {
     return UIColor(named: name.rawValue)
  }
}
