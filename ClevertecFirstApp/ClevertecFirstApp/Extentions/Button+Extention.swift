//
//  Button+Extention.swift
//  ClevertecFirstApp
//
//  Created by Александр Савков on 5.02.22.
//

import UIKit

extension UIButton {
    static func makeSystemButton(title: String) -> UIButton {
        let button = UIButton(type: .system)
        button.backgroundColor = .appColor(.button)
        button.tintColor = .white
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = button.titleLabel?.font.withSize(20)
        button.layer.cornerRadius = 22
        button.layer.borderWidth = 3
        button.layer.borderColor = UIColor.appColor(.border)?.cgColor
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}
