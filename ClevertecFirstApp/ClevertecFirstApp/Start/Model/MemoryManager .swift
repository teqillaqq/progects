//
//  MemoryManager .swift
//  ClevertecFirstApp
//
//  Created by Александр Савков on 3.02.22.
//

import Foundation

class MemoryManager {
    
    let userDefaults = UserDefaults.standard
    func savelanguage (_ lang: String?) {
        userDefaults.set(lang, forKey: "lang")
    }
    
    func loadlanguage () -> String {
        if let language = userDefaults.object(forKey: "lang") as? String {
            return language
        } else {
            return "en"
        }
    }
}
