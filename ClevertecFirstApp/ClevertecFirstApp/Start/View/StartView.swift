//
//  StartView.swift
//  ClevertecFirstApp
//
//  Created by Александр Савков on 7.02.22.
//

import UIKit

protocol StartViewInput: AnyObject {
    var language: String { get set}
    var changeThemeToLightButtonTapped: CompletionVoid? { get set }
    var changeThemeToDarkButtonTapped: CompletionVoid? { get set }
    var changeThemeToAutoButtonTapped: CompletionVoid? { get set }
    var languageChanged: CompletionVoid? { get set }
}

final class StartView: UIView {
    
    var changeThemeToLightButtonTapped: CompletionVoid?
    var changeThemeToDarkButtonTapped: CompletionVoid?
    var changeThemeToAutoButtonTapped: CompletionVoid?
    var languageChanged: CompletionVoid?
    
    lazy var languagesArray = ["English".localized(loc: language),
                               "Belorussian".localized(loc: language),
                               "Russian".localized(loc: language)]
    
    var language = MemoryManager().loadlanguage() {
        didSet {
            setLocalizate()
            pickerView.reloadAllComponents()
            MemoryManager().savelanguage(language)
        }
    }
    
    let logoImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "icon")
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var textLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.textAlignment = .center
        textLabel.adjustsFontSizeToFitWidth = true
        textLabel.text = "Welcome to ClevertecFirstApp".localized(loc: language)
        textLabel.font = .boldSystemFont(ofSize: 26)
        textLabel.numberOfLines = 2
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        return textLabel
    }()
    
    let pickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        return pickerView
    }()
    
    lazy var lightThemeButton = UIButton.makeSystemButton(title: "Light theme".localized(loc: language))
    lazy var darkThemeButton = UIButton.makeSystemButton(title: "Dark theme".localized(loc: language))
    lazy var autoThemeButton = UIButton.makeSystemButton(title: "Auto theme".localized(loc: language))
    
    private func setLocalizate() {
        textLabel.text = "Welcome to ClevertecFirstApp".localized(loc: self.language)
        lightThemeButton.setTitle("Light theme".localized(loc: language), for: .normal)
        darkThemeButton.setTitle("Dark theme".localized(loc: language), for: .normal)
        autoThemeButton.setTitle("Auto theme".localized(loc: language), for: .normal)
        languagesArray.removeAll()
        languagesArray.append("English".localized(loc: language))
        languagesArray.append("Belorussian".localized(loc: language))
        languagesArray.append("Russian".localized(loc: language))
    }
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        pickerView.dataSource = self
        pickerView.delegate = self
        setConstrains()
        setButtonTatget()
    }
    
    private func setButtonTatget() {
        lightThemeButton.addTarget(self, action: #selector(changeThemeToLight), for: .touchUpInside)
        darkThemeButton.addTarget(self, action: #selector(changeThemeToDark), for: .touchUpInside)
        autoThemeButton.addTarget(self, action: #selector(changeThemeToAuto), for: .touchUpInside)
    }
    
    @objc func changeThemeToLight() {
        overrideUserInterfaceStyle = .light
    }
    
    @objc func changeThemeToDark() {
        overrideUserInterfaceStyle = .dark
    }
    
    @objc func changeThemeToAuto() {
        overrideUserInterfaceStyle = .unspecified
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - StartsViewInput
extension StartView: StartViewInput {}

extension StartView: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        languagesArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return languagesArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch row {
        case 0:
            language = "en"
        case 1:
            language = "be"
        case 2:
            language = "ru"
        default:
            print("error")
        }
    }
}

// MARK: SetConstrains

extension StartView {
    
    private func setConstrains() {
        backgroundColor = .systemBackground
        
        addSubview(textLabel)
        NSLayoutConstraint.activate([
            textLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 5),
            textLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            textLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            textLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30)
        ])
        
        addSubview(logoImage)
        NSLayoutConstraint.activate([
            logoImage.topAnchor.constraint(equalTo: textLabel.bottomAnchor, constant: 5),
            logoImage.centerXAnchor.constraint(equalTo: centerXAnchor),
            logoImage.widthAnchor.constraint(equalToConstant: 50),
            logoImage.heightAnchor.constraint(equalToConstant: 50),
        ])
        
        addSubview(pickerView)
        NSLayoutConstraint.activate([
            pickerView.topAnchor.constraint(equalTo: textLabel.topAnchor, constant: 75),
            pickerView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        addSubview(lightThemeButton)
        addSubview(darkThemeButton)
        addSubview(autoThemeButton)
        
        NSLayoutConstraint.activate([
            lightThemeButton.topAnchor.constraint(equalTo: pickerView.bottomAnchor, constant: 0),
            lightThemeButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            lightThemeButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7),
            
            darkThemeButton.topAnchor.constraint(equalTo: lightThemeButton.bottomAnchor, constant: 5),
            darkThemeButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            darkThemeButton.widthAnchor.constraint(equalTo: lightThemeButton.widthAnchor),
            
            autoThemeButton.topAnchor.constraint(equalTo: darkThemeButton.bottomAnchor, constant: 5),
            autoThemeButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            autoThemeButton.widthAnchor.constraint(equalTo: lightThemeButton.widthAnchor)
        ])
    }
}
