//
//  StartViewController.swift
//  ClevertecFirstApp
//
//  Created by Александр Савков on 2.02.22.
//

import UIKit

final class StartViewController: UIViewController {
    
    var memoryManager = MemoryManager()
    lazy var contentView: StartViewInput = { return view as! StartViewInput }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = StartView()
        
        contentView.changeThemeToAutoButtonTapped = { [weak self] in
            self?.overrideUserInterfaceStyle = .light
        }
        contentView.changeThemeToDarkButtonTapped = { [weak self] in
            self?.overrideUserInterfaceStyle = .dark
        }
        contentView.changeThemeToLightButtonTapped = { [weak self] in
            self?.overrideUserInterfaceStyle = .unspecified
        }
        contentView.languageChanged = { [weak self] in
            self?.memoryManager.savelanguage(self?.contentView.language)
        }
        contentView.language = memoryManager.loadlanguage()
    }
}
    

    

    





